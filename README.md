# Ansible role for Spamassassin installation

## Introduction

[Spamassassin](http://spamassassin.apache.org/) is an anti-spam server.

This role installs and configure the server. It also enable Pyzor
checks.

If you need to add custom rules in special tasks/roles, you can drop
bits of configuration inside `/etc/mail/spamassassin/local.cf.d/` and
call the `regenerate spamassassin configuration` handler to add it
to the configuration.

## Variables

- **required_hits**: rules hit threshold (defaults to 5)
- **service_profile**: service "sizing" depending on expected load,
                       value in low/medium/high (defaults to low)
- **with_pyzor**: install and use the Pyzor database to detect SPAM
                  (enabled by default)
- **with_phishtank**: check in-mail URLs against the PhishTank database
                      to detect SPAM
                      (enabled by default)

